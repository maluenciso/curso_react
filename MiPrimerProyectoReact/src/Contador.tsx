import { useState } from "react"

export const Contador = () => {

    const [contador, setContador] = useState(0)

    const incrementar = () =>{
         setContador(contador+1)
    }
    const decrementar = () =>{
      setContador(contador-1)
    }
    const resetear = () =>{
      setContador(0)
    }

  return (
    <>
        <h1>Contador: {contador} </h1>
        <hr/>
        <button onClick={incrementar}>Incrementar</button>
        <button onClick={decrementar}>Decrementar</button>
        <button onClick={resetear}>Resetear</button>
    </>
   
  )
}
